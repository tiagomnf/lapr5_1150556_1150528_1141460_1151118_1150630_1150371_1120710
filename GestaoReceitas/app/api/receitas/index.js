const router     = require('express').Router();
const controller = require('./receitas.controller');
const auth       = require('../middleware/auth.middleware');


router.get('/', auth.hasRole('medico', 'utente'), controller.index);
router.get('/:id', controller.show);
router.post('/', auth.hasRole('medico'), controller.create);

router.use('/:receitaId/prescricoes', require('./prescricoes'));

module.exports = router;