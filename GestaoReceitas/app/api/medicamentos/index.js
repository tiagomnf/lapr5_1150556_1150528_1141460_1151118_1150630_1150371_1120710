const router     = require('express').Router();
const controller = require('./medicamentos.controller');

router.get('/', controller.list);
router.get('/:id/Posologias', controller.posologias);

module.exports = router;