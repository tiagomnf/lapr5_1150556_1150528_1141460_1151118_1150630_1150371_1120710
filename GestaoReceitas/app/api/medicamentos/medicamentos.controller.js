const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');

function list(req, res){
    gdm.get('/api/Medicamentos')
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

function posologias(req, res){
    gdm.get(`/api/Medicamentos/${req.params.id}/Posologias`)
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list       : list,
    posologias : posologias
};