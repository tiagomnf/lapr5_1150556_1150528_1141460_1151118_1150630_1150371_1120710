const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');

function list(req, res){
    gdm.get('/api/Farmacos')
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list : list
};