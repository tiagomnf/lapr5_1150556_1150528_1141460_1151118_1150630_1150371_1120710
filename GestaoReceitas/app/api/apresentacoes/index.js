const router     = require('express').Router();
const controller = require('./apresentacoes.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', controller.list);
router.get('/:id/Comentarios', controller.listComments);
router.post('/:id/Comentarios', controller.createComment);

module.exports = router;