const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');
const moment = require('moment');

function list(req, res){
    gdm.get('/api/Apresentacoes')
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

function listComments(req, res){
    gdm.get(`/api/Apresentacoes/${req.params.id}/Comentarios`)
    .then(response => {
        //Transform the dates from DD/MM/YYYY to YYYY/MM/DD to follow the standard in the other controllers
        const data = response.data.map(c => {
            c.data = moment(c.data, 'DD/MM/YYYY').format('YYYY/MM/DD');
            return c;
        });
        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function createComment(req, res){
    req.body.data = req.body.data || moment().format('DD/MM/YYYY');
    req.body.apresentacaoId = req.params.id;
    console.log("A criar comentario");
    gdm.post(`/api/Apresentacoes/${req.params.id}/Comentarios`, req.body)
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list          : list,
    listComments  : listComments,
    createComment : createComment
};