const router     = require('express').Router();
const controller = require('./chart.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/prescricoes', auth.isAuthenticated(),controller.prescricoes);
router.get('/chart/prescricoes/poraviar', auth.isAuthenticated(), controller.porAviar);
router.get('/prescricoes/semana', auth.isAuthenticated(), controller.prescricoes_semana);
router.get('/prescricoes/topmedicamentos', auth.isAuthenticated(), controller.top_medicamentos);

router.get('/medico/receitassemana', auth.isAuthenticated(), controller.receitas_medico_semana);
router.get('/medico/topmedicamentos', auth.isAuthenticated(), controller.top_medicamentos_medico);

module.exports = router;
