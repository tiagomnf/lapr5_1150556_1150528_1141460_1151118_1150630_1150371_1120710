const utils   = require('../../components/utils');
const Receita = require('../../models/Receita');
const moment  = require('moment');

function prescricoes(req, res) {
    Receita.find({})
    .then(receitas => {
        let data = {
            por_aviar: 0,
            parcial: 0,
            aviado: 0
        };

        var id_medico = req.user._id;

        receitas.forEach(r => {
          if(r.medico==id_medico.toString()){
            r.prescricoes.forEach(p => {
                if (p.quantidade === 0) {
                    data.aviado += 1;
                } else if (p.aviamentos.length > 0) {
                    data.parcial += 1;
                } else {
                    data.por_aviar += 1;
                }
            });
          }
        });

        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function porAviar(req, res) {
    let query = {
        utente                   : req.user,
        'prescricoes.quantidade' : {'$gt': 0}
    };
    Receita.find(query)
    .then(receitas => {
        let data = {
            hoje     : 0,
            amanha  : 0,
            esta_semana : 0,
            mais_tarde     : 0
        };
        let today = moment();

        // Filter the prescriptions
        receitas.forEach(r => {
            r.prescricoes.forEach(p => {
                if(p.quantidade == 0) return;
                let date = moment(p.dataValidade);
                if (date.isSame(today, 'day')) {
                    data.hoje += 1;
                } else if (today.clone().add(1, 'days').isSame(date, 'day')) {
                    data.amanha += 1;
                } else if (date.isBefore(today.clone().endOf('isoWeek'), 'day')) {
                    data.esta_semana += 1;
                } else {
                    data.mais_tarde += 1;
                }
            });
        });
        return res.status(200).json(data);
    }).catch(utils.handleError(req, res));
}

function prescricoes_semana(req, res) {
    Receita.find({})
    .then(receitas => {
        let data = {
            domingo: 0,
            segunda: 0,
            terca: 0,
            quarta: 0,
            quinta: 0,
            sexta: 0,
            sabado: 0
        };

          var today = new Date();


          //codigo obtido
          //https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
          var days=7;  // 7 dias antes
          var last = new Date(today.getTime() - (days * 24 * 60 * 60 * 1000));

        var id_utente = req.user._id;

        receitas.forEach(r => {

              if(r.utente==id_utente.toString()){

              r.prescricoes.forEach(p =>{

                p.aviamentos.forEach(a =>{


                    if(a.data!==null){

                      if(a.data>last){


                        weekday_value = a.data.getDay();


                        if(weekday_value==0){
                          data.domingo++;
                        }
                        if(weekday_value==1){
                          data.segunda++;
                        }
                        if(weekday_value==2){
                          data.terca++;
                        }
                        if(weekday_value==3){
                          data.quarta++;
                        }
                        if(weekday_value==4){
                          data.quinta++;
                        }
                        if(weekday_value==5){
                          data.sexta++;
                        }
                        if(weekday_value==6){
                          data.sabado++;
                        }


                      }

                    }

                })

              });


              }

        });

        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function top_medicamentos(req, res) {
    Receita.find({})
    .then(receitas => {
        let data = {
            top1: 0,
            top2: 0,
            top3: 0,
            resto: 0,
            str1: "",
            str2: "",
            str3: "",
        };


        var qtd_resto = 0;

        var vals = [];
        var strs = [];
        var cont = 0;
        var find = false ;


        var id_utente = req.user._id;

        receitas.forEach(r => {
              if(r.utente==id_utente.toString()){

              find = false;
              r.prescricoes.forEach(p =>{


                if(cont == 0){

                  vals[0] = p.quantidade ;
                  strs[0] = p.nomeMedicamento;

                  p.aviamentos.forEach(a => {

                    vals[0]=vals[0]+parseInt(a.quantidade);

                  });

                  cont ++;
                }else{

                  for (var i = 0; i < vals.length; i++) {

                    if(strs[i] == p.nomeMedicamento && find == false ) {

                      find = true;
                      vals[i]=vals[i]+p.quantidade;

                      p.aviamentos.forEach(a => {

                        vals[i]=vals[i]+parseInt(a.quantidade);

                      });

                    }

                  }


                  if(find == false){

                  vals[cont] = p.quantidade;
                  strs[cont] = p.nomeMedicamento;

                  p.aviamentos.forEach(a => {

                    vals[cont]=vals[cont]+parseInt(a.quantidade);

                  });

                  cont++;

                  }

                }

              });

              }

        });


        for (var i = 0; i < cont-1; i++) {

          for (var j = i+1; j < cont; j++) {

            if(vals[i]<vals[j]){

              var aux_val = vals[i];
              vals[i] = vals[j];
              vals[j] = aux_val;

              var aux_strs = strs [i];
              strs [i] = strs [j];
              strs [j] = aux_strs;

            }

          }

        }


        for (var i = 3; i < cont; i++) {

          data.resto = data.resto + vals[i];

        }

        data.top1 = vals[0];
        data.top2 = vals[1];
        data.top3 = vals[2];

        data.str1 =strs[0];
        data.str2 =strs[1];
        data.str3 =strs[2];

        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function receitas_medico_semana(req, res) {
    Receita.find({})
    .then(receitas => {
        let data = {
            domingo: 0,
            segunda: 0,
            terca: 0,
            quarta: 0,
            quinta: 0,
            sexta: 0,
            sabado: 0
        };

          var today = new Date();


          //codigo obtido
          //https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
          var days=7;  // 7 dias antes
          var last = new Date(today.getTime() - (days * 24 * 60 * 60 * 1000));

        var id_medico = req.user._id;



        receitas.forEach(r => {


              if(r.medico==id_medico.toString()){

                weekday_value = r.createdAt.getDay();
                console.log(weekday_value);

                if(weekday_value==0){
                  data.domingo++;
                }
                if(weekday_value==1){
                  data.segunda++;
                }
                if(weekday_value==2){
                  data.terca++;
                }
                if(weekday_value==3){
                  data.quarta++;
                }
                if(weekday_value==4){
                  data.quinta++;
                }
                if(weekday_value==5){
                  data.sexta++;
                }
                if(weekday_value==6){
                  data.sabado++;
                }


            }

        });

        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function top_medicamentos_medico(req, res) {
    Receita.find({})
    .then(receitas => {
        let data = {
            top1: 0,
            top2: 0,
            top3: 0,
            resto: 0,
            str1: "",
            str2: "",
            str3: "",
        };


        var qtd_resto = 0;

        var vals = [];
        var strs = [];
        var cont = 0;
        var find = false ;


        var id_medico = req.user._id;



        receitas.forEach(r => {

              if(r.medico==id_medico.toString()){

              find = false;
              r.prescricoes.forEach(p =>{


                if(cont == 0){

                  vals[0] = p.quantidade ;
                  strs[0] = p.nomeMedicamento;

                  p.aviamentos.forEach(a => {

                    vals[0]=vals[0]+parseInt(a.quantidade);

                  });

                  cont ++;
                }else{

                  for (var i = 0; i < vals.length; i++) {

                    if(strs[i] == p.nomeMedicamento && find == false ) {

                      find = true;
                      vals[i]=vals[i]+p.quantidade;

                      p.aviamentos.forEach(a => {

                        vals[i]=vals[i]+parseInt(a.quantidade);

                      });

                    }

                  }


                  if(find == false){

                  vals[cont] = p.quantidade;
                  strs[cont] = p.nomeMedicamento;

                  p.aviamentos.forEach(a => {

                    vals[cont]=vals[cont]+parseInt(a.quantidade);

                  });

                  cont++;

                  }

                }

              });

              }

        });


        for (var i = 0; i < cont-1; i++) {

          for (var j = i+1; j < cont; j++) {

            if(vals[i]<vals[j]){

              var aux_val = vals[i];
              vals[i] = vals[j];
              vals[j] = aux_val;

              var aux_strs = strs [i];
              strs [i] = strs [j];
              strs [j] = aux_strs;

            }

          }

        }



        for (var i = 3; i < cont; i++) {

          data.resto = data.resto + vals[i];

        }

        data.top1 = vals[0];
        data.top2 = vals[1];
        data.top3 = vals[2];

        data.str1 =strs[0];
        data.str2 =strs[1];
        data.str3 =strs[2];

        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}



module.exports = {
    prescricoes : prescricoes,
    porAviar    : porAviar,
    prescricoes_semana : prescricoes_semana,
    top_medicamentos : top_medicamentos,
    receitas_medico_semana : receitas_medico_semana,
    top_medicamentos_medico : top_medicamentos_medico
};
