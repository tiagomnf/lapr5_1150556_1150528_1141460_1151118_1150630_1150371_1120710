const router     = require('express').Router();
const controller = require('./utente.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/prescricoes/poraviar', controller.prescricoes);

module.exports = router;