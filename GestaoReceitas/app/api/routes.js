const router = require('express').Router();              // get an instance of the express Router
const auth   = require('./middleware/auth.middleware');

// ROUTES FOR OUR API
// =============================================================================
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Hooray! Welcome to our api!' });
});

router.use('/auth', require('./auth'));
router.use('/Apresentacoes', require('./Apresentacoes'));
router.use('/chart', require('./chart'));
router.use('/Farmacos', auth.isAuthenticated(), auth.hasRole('medico'), require('./Farmacos'));
router.use('/Medicamentos', auth.isAuthenticated(), auth.hasRole('medico'), require('./Medicamentos'));
router.use('/receitas', auth.isAuthenticated(), require('./receitas'));
router.use('/utentes', auth.isAuthenticated(), auth.hasRole('utente'), require('./utente'));
router.use('/utilizadores', auth.isAuthenticated(), require('./utilizadores'));

module.exports = router;