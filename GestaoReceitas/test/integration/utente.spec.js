const Receita  = require('../../app/models/Receita');
const User     = require('../../app/models/User');
const chai     = require('chai');
const chaiHttp = require('chai-http');
const server   = require('../../server');
const expect   = chai.expect;
const assert   = chai.assert;
const moment   = require('moment');
chai.use(chaiHttp);

let data = require('../data.json');

describe('/api/utentes', function(){
    describe('GET /prescricoes/poraviar', function(){
        before(function(){
            //Create the users 
            return User.remove({})
            .then(() => {
                return User.create([data.medico, data.utente]);
            })
            .then(response => {
                data.medico._id       = response[0]._id;
                data.utente._id       = response[1]._id;

                data.receita.medico = data.medico;
                data.receita.utente = data.utente;

                data.receita2.utente = data.medico;
                data.receita2.medico = data.medico;

                //Login
                return chai.request(server)
                .post('/api/auth/login')
                .send({email: data.utente.email, password: data.utente.password});
            })
            .then(user => {
                data.utente.token = user.body.token;
            });
        });

        beforeEach(function(){
            //Reset receita's document
            return Receita.remove({})
            .then(receita => {
                return Promise.all([Receita.create(data.receita), Receita.create(data.receita2)]);
            })
            .then(receitas => {
                data.receita._id = receitas[0]._id;
                data.receita.prescricoes[0]._id = receitas[0].prescricoes[0]._id;
                data.receita.prescricoes[1]._id = receitas[0].prescricoes[1]._id;
            });
        });

        it('lists all prescriptions for a given user', function(){
            return chai.request(server)
            .get(`/api/utentes/prescricoes/poraviar`)
            .set('Authorization', 'Bearer '+ data.utente.token)
            .then(res => {
                expect(res).to.have.status(200);

                expect(res.body).to.be.an('array').that.has.lengthOf(2);
                expect(res.body[0]).to.have.property('_id', data.receita.prescricoes[0]._id.toString());
                expect(res.body[1]).to.have.property('_id', data.receita.prescricoes[1]._id.toString());
            });
        });

        it('also filters prescriptions by date', function(){
            let searchDate = moment().add(1, 'days');
            return Receita.find({'prescricoes._id': data.receita.prescricoes[0]._id})
            .update({'prescricoes.$.dataValidade': searchDate})
            .then(updated => {
                return chai.request(server)
                .get(`/api/utentes/prescricoes/poraviar?data=${searchDate.toISOString()}`)
                .set('Authorization', 'Bearer '+ data.utente.token);
            })
            .then(response => {
                expect(response.body).to.be.an('array').that.has.lengthOf(1);

                expect(response.body[0]).to.have.property('_id', data.receita.prescricoes[0]._id.toString());
            });
            
        });

        it('excludes prescricoes that have no medicines remaining', function(){
            return Receita.find({'prescricoes._id': data.receita.prescricoes[0]._id})
            .update({'prescricoes.$.quantidade': 0})
            .then(updated => {
                return chai.request(server)
                .get(`/api/utentes/prescricoes/poraviar`)
                .set('Authorization', 'Bearer '+ data.utente.token);
            })
            .then(response => {
                expect(response.body).to.be.an('array').that.has.lengthOf(1);
 
                expect(response.body[0]).to.have.property('_id', data.receita.prescricoes[1]._id.toString());
            });
        });
    });
});

