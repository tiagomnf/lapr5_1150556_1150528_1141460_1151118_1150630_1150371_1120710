const Receita  = require('../../app/models/Receita');
const User     = require('../../app/models/User');
const chai     = require('chai');
const chaiHttp = require('chai-http');
const server   = require('../../server');
const expect   = chai.expect;
const moment   = require('moment');
const authService = require('../../app/api/auth/auth.service');

chai.use(chaiHttp);

let data = require('../data.json');

describe('/api/receitas/:receita/prescricoes', function(){
    before(function(){
        //Create the users 
        return User.remove({})
        .then(() => {
            return User.create([data.medico, data.farmaceutico, data.utente]);
        })
        .then(response => {
            data.medico._id       = response[0]._id;
            data.farmaceutico._id = response[1]._id;
            data.utente._id       = response[2]._id;
            
            data.receita.medico = data.medico._id;
            data.receita.utente = data.utente._id;
            
            //Login
            data.farmaceutico.token = authService.signToken(data.farmaceutico).token;
            data.utente.token = authService.signToken(data.utente).token;
            data.medico.token = authService.signToken(data.medico).token;
        });
    });
    describe('PUT /:prescricao/aviar', function(){
        beforeEach(function(){
            //Reset receita's document
            return Receita.remove({})
            .then(receita => {
                return Receita.create(data.receita);
            })
            .then(receita => {
                data.receita._id = receita._id;
                data.receita.prescricoes[0]._id = receita.prescricoes[0]._id;
            });
        });
        
        it('should not allow unauthenticated users', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(401);
                done();
            });
        });
        
        it('should not authorize users', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.utente.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not authorize doctors', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.medico.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should validate the existence of input', function(){
            return chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.farmaceutico.token)
            .send()
            .catch(err => {
                expect(err).to.have.status(422);
            });
        });
        
        it('should respond with 404 when receita does not exist', function(){
            return chai.request(server)
            .put(`/api/receitas/random/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.farmaceutico.token)
            .send({quantidade: 1})
            .catch(err => {
                expect(err).to.have.status(404);
            });
        });
        
        it('should respond with 404 when prescricao does not exist', function(){
            return chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/random/aviar`)
            .set('Authorization', 'Bearer '+ data.farmaceutico.token)
            .send({quantidade: 1})
            .catch(err => {
                expect(err).to.have.status(404);
            });
        });
        
        it('should not dispatch when the ammount is above the limit', function(){
            return chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.farmaceutico.token)
            .send({quantidade: 6})
            .catch(res => {
                expect(res).to.have.status(422);
            });
        });
        
        it('should not dispatch when the date is in the past', function(){
            let prescricao = data.receita.prescricoes[0]._id;
            return Receita.update({'prescricoes._id': prescricao }, {'$set' : {'prescricoes.$.dataValidade': moment().subtract(1, 'days')}}, {runValidators: false})
            .then(() => {
                return chai.request(server)
                .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
                .set('Authorization', 'Bearer '+ data.farmaceutico.token)
                .send({quantidade: 3});
            })
            .catch(res => {
                expect(res).to.have.status(422);
            });
        });
        
        it('should dispatch and create a new record when everything is correct', function(){
            this.timeout(10000);
            return chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}/aviar`)
            .set('Authorization', 'Bearer '+ data.farmaceutico.token)
            .send({quantidade: 3})
            .then(res => {
                expect(res).to.have.status(200);
                
                return Receita.findById(data.receita._id);
            })
            .then(receita => {
                expect(receita).to.have.property('prescricoes');
                expect(receita.prescricoes).to.be.an('array').that.has.lengthOf(2);
                
                let prescricao = receita.prescricoes[0];
                expect(prescricao).to.have.property('quantidade', 2);
                
                expect(prescricao).to.have.property('aviamentos');
                expect(prescricao.aviamentos).to.be.an('array').that.has.lengthOf(1);
                
                let aviamento = prescricao.aviamentos[0];
                expect(aviamento).to.have.property('quantidade', 3);
            });
        });
    });
    
    describe('POST /', function(){
        beforeEach(function(){
            //Reset receita's document
            return Receita.remove({})
            .then(receita => {
                data.receita.medico = data.farmaceutico._id;
                return Receita.create(data.receita);
            })
            .then(receita => {
                data.receita._id = receita._id;
                data.receita.prescricoes[0]._id = receita.prescricoes[0]._id;
            });
        });
        it('should not allow unauthenticated users', function(done){
            chai.request(server)
            .post(`/api/receitas/${data.receita._id}/prescricoes`)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(401);
                done();
            });
        });
        
        it('should not authorize users', function(done){
            chai.request(server)
            .post(`/api/receitas/${data.receita._id}/prescricoes`)
            .set('Authorization', 'Bearer ' + data.utente.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not authorize pharmaceutists', function(done){
            chai.request(server)
            .post(`/api/receitas/${data.receita._id}/prescricoes`)
            .set('Authorization', 'Bearer ' + data.farmaceutico.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not allow doctors if they are the not owner of the recipe', function(done){
            chai.request(server)
            .post(`/api/receitas/${data.receita._id}/prescricoes`)
            .set('Authorization', 'Bearer ' + data.medico.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should add a prescription', function(){
            let prescricao = {
                idMedicamento  : 1,
                idApresentacao : 1,
                posologia      : "2x por dia",
                quantidade     : 2,
                dataValidade   : moment().add(5, 'days')
            };
            return Receita.findById(data.receita._id)
            .update({medico: data.medico._id})
            .then(updated => {
                return chai.request(server)
                .post(`/api/receitas/${data.receita._id}/prescricoes`)
                .set('Authorization', 'Bearer ' + data.medico.token)
                .send(prescricao);
            })
            .then((res) => {
                expect(res).to.have.status(201);
                
                expect(res.body).to.have.property('quantidade', prescricao.quantidade);
                expect(res.body).to.have.property('idMedicamento', prescricao.idMedicamento);
                expect(res.body).to.have.property('idApresentacao', prescricao.idApresentacao);
                expect(res.body).to.have.property('_id');
                
                return Receita.findById(data.receita._id);
            })
            .then(receita => {
                expect(receita.prescricoes).to.be.an('array').that.has.lengthOf(3);
            });
        });
    });
    
    describe('PUT /:prescricao', function(){
        beforeEach(function(){
            //Reset receita's document
            return Receita.remove({})
            .then(receita => {
                data.receita.medico = data.farmaceutico._id;
                return Receita.create(data.receita);
            })
            .then(receita => {
                data.receita._id = receita._id;
                data.receita.prescricoes[0]._id = receita.prescricoes[0]._id;
            });
        });
        it('should not allow unauthenticated users', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(401);
                done();
            });
        });
        
        it('should not authorize users', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
            .set('Authorization', 'Bearer ' + data.utente.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not authorize pharmaceutists', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
            .set('Authorization', 'Bearer ' + data.farmaceutico.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not allow doctors if they are the not owner of the recipe', function(done){
            chai.request(server)
            .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
            .set('Authorization', 'Bearer ' + data.medico.token)
            .send()
            .end((err, res) => {
                expect(err).to.not.be.null;
                expect(res).to.have.status(403);
                done();
            });
        });
        
        it('should not allow to change if the prescription was already taken', function(done){
            Receita.findById(data.receita._id)
            .update({'prescricoes._id': data.receita.prescricoes[0]._id}, {
                '$set': {
                    'prescricoes.$.aviamentos' : [{quantidade: 1}],
                    'medico'                   : data.medico._id
                }
            }).then(updated => {
                chai.request(server)
                .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
                .set('Authorization', 'Bearer ' + data.medico.token)
                .send({})
                .end((err, res) => {
                    expect(err).to.not.be.null;
                    expect(res).to.have.status(403);
                    done();
                });
            });
        });
        
        it('should change the prescription', function(){
            let old             = data.receita.prescricoes[0];
            let newP            = old;
                newP.quantidade = 15;

            return Receita.findById(data.receita._id)
            .update({medico: data.medico._id})
            .then(updated => {
                return chai.request(server)
                .put(`/api/receitas/${data.receita._id}/prescricoes/${data.receita.prescricoes[0]._id}`)
                .set('Authorization', 'Bearer ' + data.medico.token)
                .send(newP);
            })
            .then(res => {
                expect(res).to.have.status(200);

                return Receita.findById(data.receita._id);
            })
            .then(receita => {
                let prescricao = receita.prescricoes.id(data.receita.prescricoes[0]._id);
                expect(prescricao).to.have.property('quantidade', 15);
            });
        });
    });
});

