// call the packages we need
const express = require('express');                   // call express
const cors = require('cors');
var app     = express();                            // define our app using express
const config  = require('./app/config/environment');
const http    = require('http').Server(app);
const websocket = require('./app/components/websocket');

var fs = require('fs');
var morgan = require('morgan');
var path = require('path');

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});
 
// setup the logger
app.use(morgan('tiny', {stream: accessLogStream}));

//app.use(cors());
//Configure the app
require('./app/config')(app);

// Initialize the websocket component
websocket.init(http);


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', require('./app/api/routes.js'));


// START THE SERVER
// =============================================================================
let server = http.listen(config.port, () => {
    console.log('Express server listening on %d, in %s mode', config.port, config.env);
});

module.exports = server; //For testing