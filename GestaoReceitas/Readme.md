# Gestão de receitas

## Índice
* [Instalação](#setup)
* [Usage](#usage)
* [Configuration](#configuration)
* [Routes](#routes)
## Setup

```bash
git clone <url>
cd <pasta>
npm install
```

## Usage
A aplicação está preparada para usar gulp.

**Para executar a aplicação:**
```bash
gulp serve
```
Este comando inicia a aplicação na porta especificada nos ficheiros de configuração e adiciona um _watcher_. Sempre que se modificar algum ficheiro a aplicação reinicia automaticamente.

**Para executar os testes:**
```bash
gulp test
```
ou 
```bash
gulp watch:test
```

## Configuration
Todas as configurações da aplicação estão guardadas na pasta _app/config/environment_. Devem alterar os ficheiros para ficarem apropriados às vossas necessidades.

## Routes
### Designações:
**M** -> Médico
**U** -> Utente
**F** -> Farmacêutico
**A** -> Apenas autenticado
**--** -> Sem autenticação

### Como enviar o token?
É so colocar o token nos headers:
```curl
Authorization: Bearer <token>
```

### Autenticação
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------|
| Autenticação  |                                              |         |                          |
| POST          | /auth/register                               | --      | Registar um utilizador. É necessário nome, email, password e papel (role) |
| POST          | /auth/login                                  | --      |                      |

### Receitas
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------|  
| GET           | /receitas                   | M, U          | Lista de receitas baseado no utilizador. Médico só vê as receitas criadas por si. Utente só vê as receitas criadas para si. |
| POST          | /receitas                                    | M       | Criar uma receita. Aqui deve-se enviar todos os dados da receita, incluindo pelo menos uma prescrição |
| GET           | /receitas/:id                                | M, U, F | Ver informações da receita |

### Prescrições
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /receitas/:id/prescricoes/:prescricao        | M, U, F | Ver prescrição de uma receita                                                                                                                       |
| POST          | /receitas/:id/prescricoes                    | M       | Criar uma prescrição para a receita :id  |
| PUT           | /receitas/:id/prescricoes/:prescricao        | M       | Editar a prescrição. Só devem ser enviados os dados necessários. O que não for enviado não é alterado.  |
| PUT           | /receitas/:id/prescricoes/:prescricoes/aviar | F       | Avia uma prescrição. Só é necessário colocar a quantidade.                                                                                          |

### Utentes
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /utente/prescricoes/poraviar{?data=}          | U       | Prescrições por aviar do utente que fez o pedido. Opcionalmente pode ser enviada uma data que vai restringir apenas as prescrições antes dessa data |

### Utilizadores
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /utilizadores{?role=}        | A       | Lista de utilizadores. Pode ser enviado um _query_ para filtrar por papel |

### Apresentações
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /apresentacoes                               | --      | Lista de apresentações  |
| GET           | /apresentacoes/:id/comentarios               | --      | Comentários da apresentação  |
| POST          | /apresentacoes/:id/comentarios               | M       | Criar comentário na apresentação. Só é necessário enviar o texto  |

### Fármacos
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /farmacos                                    | M       | Lista de fármacos       |

### Medicamentos
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /medicamentos                                | M       | Lista de medicamentos    |
| GET           | /medicamentos/:id/posologias                 | M       | Posologias do medicamento     |

### Chart
|      Método         |    URL                |    Quem?      |       Descrição    |
|---------------------|-----------------------|---------------|---------------------| 
| GET           | /chart/prescricoes                           | --      | Informação necessрria para construir o gráfico                                                                                                      |
| GET           | /chart/prescricoes/poraviar                  | U       | Informações necessрrias para construir o gráfico do utente  |
