import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Receita } from '../../models/index';
import { Prescricao } from '../../models/Prescricao';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit { 
    
    pies  = {
        prescricoes: {
            labels: ["Por aviar", "Parcialmente aviadas", "Completamente aviadas"],
            data: [0, 0, 0],
            loading: false
        },
        utente: {
            labels: ["Hoje", "Amanhã", "Esta semana", "Depois"],
            data: [0, 0, 0, 0],
            loading: false
        }
    };
    constructor(
        private http: HttpClient,
        public authService: AuthService,
        private toastr: ToastrService
    ) {}
    
    ngOnInit() {
        this.loadData();
    }
    
    loadData() {
        this.loadPrescriptions();
        
        if (this.authService.hasRole('utente')) {
            this.loadUserPrescriptions();
        }
    }
    
    private loadPrescriptions() {
        this.pies.prescricoes.loading = true;
        this.http.get<any>('chart/prescricoes').subscribe(
            response => {
                let data = [ response.por_aviar, response.parcial, response.aviado];
                // Need to replace the original array otherwise the chart wont update
                this.pies.prescricoes.data = data;
                this.pies.prescricoes.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.prescricoes.loading = false;
            }
        );
    }
    
    private loadUserPrescriptions() {
        this.pies.utente.loading = true;
        this.http.get<Prescricao[]>('utentes/prescricoes/poraviar')
        .subscribe(
            prescricoes => {
                let today = moment();
                let data = [0, 0, 0, 0];
                
                prescricoes.forEach(p => {
                    let date = moment(p.dataValidade);
                    if (date.isSame(today, 'day')) {
                        data[0] += 1;
                    } else if (today.clone().add(1, 'days').isSame(date, 'day')) {
                        data[1] += 1;
                    } else if (today.clone().endOf('week').isAfter(date, 'day')) {
                        data[2] += 1;
                    } else {
                        data[3] += 1;
                    }
                });
                
                this.pies.utente.data = data;
                this.pies.utente.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.utente.loading = false;
            }
        );
    }
}
