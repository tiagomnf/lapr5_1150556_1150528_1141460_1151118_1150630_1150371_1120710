import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PrescriptionDetailsComponent } from '../../dialogs/prescription-details/prescription-details.component';
import { Prescricao, Receita } from '../../models';
import { AuthService } from '../../services/auth.service';


@Component({
    selector: 'app-receipts',
    templateUrl: 'receipts.component.html',
    styleUrls: ['./receipts.component.scss']
})  
export class ReceiptsComponent implements OnInit { 
    receitas: Array<Receita>;
    bsModalRef : BsModalRef;
    loading: boolean = false;
    
    constructor(
        private http: HttpClient, 
        private toastr: ToastrService, 
        private modalService: BsModalService, 
        public authService: AuthService
    ) {}
    
    ngOnInit() {
        this.getReceipts();
    }
    
    getReceipts() {
        this.loading = true;
        this.http.get<Receita[]>('receitas')
        .subscribe(
            response => {
                this.receitas = response;
                this.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.loading = false;
            }
        );
    }
    
    showPrescription(prescription: Prescricao) {
        this.bsModalRef = this.modalService.show(PrescriptionDetailsComponent, {class: 'modal-lg'});
        this.bsModalRef.content.prescricoes = prescription;
    }
    
}
