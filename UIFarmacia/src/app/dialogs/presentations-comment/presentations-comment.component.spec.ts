import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentationsCommentComponent } from './presentations-comment.component';

describe('PresentationsCommentComponent', () => {
  let component: PresentationsCommentComponent;
  let fixture: ComponentFixture<PresentationsCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentationsCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentationsCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
