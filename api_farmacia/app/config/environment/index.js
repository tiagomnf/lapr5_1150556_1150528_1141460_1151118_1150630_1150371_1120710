const _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
let all = {
    env: env,
    
    // Server port
    port: process.env.PORT || 8080,
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://1150528:casadei@ds249005.mlab.com:49005/projeto_arqsi'
    },

    jwt: {
        secret   : "fSk35bzq6KutR0dQVKTL",
        issuer   : "http://projeto.arqsi.local",
        audience : "Everyone"
    },

    gestaoMedicamentos: {
        url      : 'http://lapr56616.azurewebsites.net'
        // email    : 'email@example.com',
        // password : 'password'
    },

    mail: {
        host     : 'mail.smtp2go.com',
        port     : 2525,
        username : 'smtp_username',
        password : 'smtp_password'
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`)
);