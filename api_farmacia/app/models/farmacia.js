var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var FarmaciaSchema   = new Schema({
    local: String,
    nome: String,
    node: String,
    farmaceutico: String,
    medicamentos: [{
        id_medicamento: String,
	       nome: String,
        stock: Number,
        encomenda: Boolean,
    }]    
},{ versionKey: false });

module.exports = mongoose.model('Farmacia', FarmaciaSchema);
