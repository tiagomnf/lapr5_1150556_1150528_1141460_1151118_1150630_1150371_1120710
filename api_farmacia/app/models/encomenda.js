

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var EncomendaSchema   = new Schema({
    farmacia: String,
    horario: String,
    medicamento: String,
    qnt: Number,
    entregue: Boolean
},{ versionKey: false });

module.exports = mongoose.model('Encomenda', EncomendaSchema);

