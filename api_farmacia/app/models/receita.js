

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ReceitaSchema   = new Schema({
    utente: {type: Schema.ObjectId, ref: 'User'},
    medico: {type: Schema.ObjectId, ref: 'User'},
    prescricoes: [{
        farmaco: String,
        apresentacao: String,
        posologia: String,
        quantidade: Number,
        validade: Date,
        aviamento: [{
            data: {type: Date, default: Date.now},
            farmaceutico: {type: Schema.ObjectId, ref: 'User'}
        }]
    }]
    
    
    
},{ versionKey: false });

module.exports = mongoose.model('Receita', ReceitaSchema);
