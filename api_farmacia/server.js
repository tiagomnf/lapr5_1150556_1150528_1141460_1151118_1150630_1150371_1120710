// server.js

// BASE SETUP
// =============================================================================


//mongodb://<dbuser>:<dbpassword>@ds042417.mlab.com:42417/arqsi

// call the packages we need
var cors = require('cors');
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Farmacia = require('./app/models/farmacia');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var config = require('./config');
var User = require('./app/models/user');
var Receita = require('./app/models/receita');
var Encomenda = require('./app/models/encomenda');
var request = require('request');
var nodemailer = require('nodemailer');
var schedule = require('node-schedule');

//mongodb://mcn:mcn@ds121575.mlab.com:21575/arqsi2 // connection string para MongoDB local
//mongodb://<dbuser>:<dbpassword>@ds161346.mlab.com:61346/lapr5
//mongodb://<dbuser>:<dbpassword>@ds249005.mlab.com:49005/projeto_arqsi
mongoose.connect('mongodb://admin:admin123@ds249005.mlab.com:49005/projeto_arqsi');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!");
});
mongoose.Promise = global.Promise;



app.use(cors());

//var Prescricao     = require('./app/models/prescricao');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080; // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router




// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);


function hasRole(userEmail, role, func) {
    User.findOne({
        email: userEmail
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.json({
                sucess: false,
                message: 'Auth failed!'
            });

        } else if (user) {
            func(role === 'medico' && user.medico === true);
        }
    })
}

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();

// get an instance of the express Router


router.use('/auth', require('./app/auth'));

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});


//todos os dias as 24h faz levantamento de encomendas para entregar de manha e envia email ao fornecedor com a rota
var j = schedule.scheduleJob('00 00 00 * * *', function(){
    
    
       Encomenda.find(function (err, encomendas) {

        var encomendas2 = [];


        var i = 0;
        encomendas.forEach(function (element) {

            if (element.entregue === false && element.horario === 'manha') {

                encomendas2[i] = element.farmacia;
                i++;

            }
        }, this);

        var i = 0;
        var teste = '[';

        var i = 0;
        encomendas2.forEach(function (element) {

            teste = teste + `"${encomendas2[i]}",`;
            i++;

        }, this);

        teste = teste + ']';


        var stringbody = `{"paragens" : ${teste}}`;


        if (err)
            res.send(err);



        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/json'
        }

        var options = {
            method: 'POST',
            url: 'http://54.186.67.253:80/',
            headers: headers,
            body: stringbody,
            Json: true

        };
        //console.log(options);
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //print the response


                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'lapr5inf@gmail.com',
                        pass: 'Lapr_2017'
                    }
                });

                var mailOptions = {
                    from: 'lapr5inf@gmail.com',
                    to: 'marcosoareslei@gmail.com',
                    subject: 'Lista de encomendas para manha',
                    text: body
                };

                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });




                if (err) {
                    console.log(err);

                }


            } else {
                console.log(body);
            }
        });

        //res.json(encomendas2);
    });
    
    

});




//todos os dias as 12h faz levantamento de encomendas para entregar de tarde e envia email ao fornecedor com a rota
var h = schedule.scheduleJob('00 00 12 * * *', function(){
    
    
       Encomenda.find(function (err, encomendas) {

        var encomendas2 = [];


        var i = 0;
        encomendas.forEach(function (element) {

            if (element.entregue === false && element.horario === 'tarde') {

                encomendas2[i] = element.farmacia;
                i++;

            }
        }, this);

        var i = 0;
        var teste = '[';

        var i = 0;
        encomendas2.forEach(function (element) {

            teste = teste + `"${encomendas2[i]}",`;
            i++;

        }, this);

        teste = teste + ']';


        var stringbody = `{"paragens" : ${teste}}`;


        if (err)
            res.send(err);



        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/json'
        }

        var options = {
            method: 'POST',
            url: 'http://54.186.67.253:80/',
            headers: headers,
            body: stringbody,
            Json: true

        };
        //console.log(options);
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //print the response


                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'lapr5inf@gmail.com',
                        pass: 'Lapr_2017'
                    }
                });

                var mailOptions = {
                    from: 'lapr5inf@gmail.com',
                    to: 'marcosoareslei@gmail.com',
                    subject: 'Lista de encomendas para tarde',
                    text: body
                };

                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });




                if (err) {
                    console.log(err);

                }


            } else {
                console.log(body);
            }
        });

        //res.json(encomendas2);
    });
    
    

});



app.get('/favicon.ico', function(req, res) {
    res.status(204);
});


//criar/get farmacia
//adicionar medicamento a farmacia

//
router.route('/farmacia')
    // create farmacia
    .post(function (req, res) {

        var farmacia = new Farmacia(); // create a new instance of the 
        farmacia.local = req.body.local;
        farmacia.node = req.body.node;
        farmacia.nome = req.body.nome;
        farmacia.farmaceutico = req.body.farmaceutico;
        farmacia.medicamentos = req.body.medicamentos;


        // save receita
        farmacia.save(function (err) {
            if (err)
                res.send(err);

            res.json({
                message: 'farmacia criado!'
            });
        });

    })

// get farmacia
.get(function (req, res) {
    Farmacia.find(function (err, farmacias) {
        if (err)
            res.send(err);

        res.json(farmacias);
    });
});



router.route('/encomenda')
    // create encomenda
    .post(function (req, res) {

        Farmacia.findOne({
            farmaceutico: req.body.farmaceutico
        }, function (err, farmacia) {

            var encomenda = new Encomenda(); // create a new instance of the 
            encomenda.farmacia = farmacia.node;
            encomenda.nome = req.body.nome;
            encomenda.horario = req.body.horario;
            encomenda.medicamento = req.body.medicamento;
            encomenda.qnt = req.body.qnt;
            encomenda.entregue = false;


            // save receita
            encomenda.save(function (err) {
                if (err)
                    res.send(err);

                res.json({
                    message: 'encomenda criada!'
                });
            });
        });

    })

// get encomenda
.get(function (req, res) {
    Encomenda.find(function (err, encomendas) {

        var encomendas2 = [];


        var i = 0;
        encomendas.forEach(function (element) {

            if (element.entregue === false) {

                encomendas2[i] = element;
                i++;

            }
        }, this);


        if (err)
            res.send(err);

        res.json(encomendas2);
    });
});




router.route('/receita/:receita_id')

// get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
.get(function (req, res) {

    Receita.findById(req.params.receita_id, function (err, receita) {
        if (err)
            res.send("A Receita nao existe!");
        res.json(receita);

    })

});






//put aviar medicamentos (retira x quantidade do medicamento) id_medicamento, quantidade, id_Facmaceutico
router.route('/farmacia/aviar')

.put(function (req, res) {


    Farmacia.findOne({
        farmaceutico: req.body.farmaceutico_id
    }, function (err, farmacia) {
        var i = 0;
        var a = false;

        if (err)
            res.send(err);


        for (i = 0; i < farmacia.medicamentos.length; i++) {


            if (farmacia.medicamentos[i].id_medicamento == req.body.medicamento_id) {
                var quantidade = req.body.quantidade

                farmacia.medicamentos[i].stock = farmacia.medicamentos[i].stock - quantidade;
                a = true;

            }
        }



        if (a == true) {
            farmacia.save(function (err) {

                if (err)
                    res.send(err);

                res.json({
                    message: 'medicamento atualizada'
                });
            });

        } else {
            res.json({
                message: 'ERRO!! medicamento'
            });
        }


    });


});



// encomendas manha
router.route('/encomendasmanha')


// get encomendas manha
.get(function (req, res) {
    Encomenda.find(function (err, encomendas) {

        var encomendas2 = [];


        var i = 0;
        encomendas.forEach(function (element) {

            if (element.entregue === false && element.horario === 'manha') {

                encomendas2[i] = element.farmacia;
                i++;

            }
        }, this);

        var i = 0;
        var teste = '[';

        var i = 0;
        encomendas2.forEach(function (element) {

            teste = teste + `"${encomendas2[i]}",`;
            i++;

        }, this);

        teste = teste + ']';


        var stringbody = `{"paragens" : ${teste}}`;


        if (err)
            res.send(err);



        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/json'
        }

        var options = {
            method: 'POST',
            url: 'http://54.186.67.253:80/',
            headers: headers,
            body: stringbody,
            Json: true

        };
        //console.log(options);
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //print the response


                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'lapr5inf@gmail.com',
                        pass: 'Lapr_2017'
                    }
                });

                var mailOptions = {
                    from: 'lapr5inf@gmail.com',
                    to: 'marcosoareslei@gmail.com',
                    subject: 'Lista de encomendas para manhã',
                    text: body
                };

                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });




                if (err) {
                    console.log(err);

                }


            } else {
                console.log(body);
            }
        });

        //res.json(encomendas2);
    });
});




// encomendas tarde

router.route('/encomendastarde')

// get encomendas manha
.get(function (req, res) {
    Encomenda.find(function (err, encomendas) {

        var encomendas2 = [];


        var i = 0;
        encomendas.forEach(function (element) {

            if (element.entregue === false && element.horario === 'tarde') {

                encomendas2[i] = element.farmacia;
                i++;

            }
        }, this);

        var i = 0;
        var teste = '[';

        var i = 0;
        encomendas2.forEach(function (element) {

            teste = teste + `"${encomendas2[i]}",`;
            i++;

        }, this);

        teste = teste + ']';


        var stringbody = `{"paragens" : ${teste}}`;


        if (err)
            res.send(err);



        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/json'
        }

        var options = {
            method: 'POST',
            url: 'http://54.186.67.253:80/',
            headers: headers,
            body: stringbody,
            Json: true

        };
        //console.log(options);
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //print the response


                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'lapr5inf@gmail.com',
                        pass: 'Lapr_2017'
                    }
                });

                var mailOptions = {
                    from: 'lapr5inf@gmail.com',
                    to: 'marcosoareslei@gmail.com',
                    subject: 'Lista de encomendas para tarde',
                    text: body
                };

                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });




                if (err) {
                    console.log(err);

                }


            } else {
                console.log(body);
            }
        });

        //res.json(encomendas2);
    });
});





// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);