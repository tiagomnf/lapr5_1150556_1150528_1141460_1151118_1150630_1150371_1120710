export class Apresentacao {
    id           : string;
    nome         : string;
    forma        : string;
    concentracao : string;
    quantidade   : string;
    farmacoId    : number;
}
