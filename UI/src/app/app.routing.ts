import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Guards
import { AuthGuard } from './guards/auth.guard';

import { P404Component } from './pages/404.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Início'
        },
        children: [
            {
                path: 'home',
                loadChildren: './pages/home/home.module#HomeModule',
            },
            {
                path: 'receitas',
                canActivateChild: [AuthGuard],
                loadChildren: './pages/receipts/receipts.module#ReceiptsModule'
            },
            {
                path: 'apresentacoes',
                loadChildren: './pages/presentations/presentations.module#PresentationsModule',
            },
            {
                path: 'prescricoes',
                canActivate: [AuthGuard],
                data: {
                    requiredRole: 'utente'
                },
                loadChildren: './pages/prescriptions/prescriptions.module#PrescriptionsModule',
            },
            {
                path: 'extras',
                canActivate: [AuthGuard],
                data: {
                    requiredRole: 'utente'
                },

                loadChildren: './pages/extras/extras.module#ExtrasModule',

            },
            {
                path: 'extras-m',
                canActivate: [AuthGuard],
                data: {
                    requiredRole: 'medico'
                },

                loadChildren: './pages/extras-m/extras-m.module#ExtrasMModule',

            },
        ]
    },
    {
        path: 'auth',
        component: SimpleLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './pages/auth/auth.module#AuthModule',
            }
        ]
    },
    { path: 'not-found', component: P404Component },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ],
    declarations: [P404Component]
})
export class AppRoutingModule {}
