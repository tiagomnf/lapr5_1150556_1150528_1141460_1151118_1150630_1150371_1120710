import { Component, OnInit } from '@angular/core';
import { Prescricao } from '../../models/Prescricao';
import { BsModalRef } from 'ngx-bootstrap';
import * as moment from 'moment';

@Component({
    selector: 'app-modal-prescription-details',
    templateUrl: './prescription-details.component.html',
    styleUrls: ['./prescription-details.component.scss']
})

export class PrescriptionDetailsComponent implements OnInit {
    prescricoes     : Array<Prescricao> = [];

    constructor(public bsModalRef: BsModalRef) { 
        
    }
    
    ngOnInit() {

    }
    
}
