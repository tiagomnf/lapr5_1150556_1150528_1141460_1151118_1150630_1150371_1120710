import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchPrescriptionComponent } from './dispatch-prescription.component';

describe('DispatchPrescriptionComponent', () => {
  let component: DispatchPrescriptionComponent;
  let fixture: ComponentFixture<DispatchPrescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchPrescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchPrescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
