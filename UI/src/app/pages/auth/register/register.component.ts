import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  templateUrl: 'register.component.html'
})

export class RegisterComponent {

  terms: any;
  errors: Array<string> = [];
  email: string = '';
  name: string = '';
  password: string = '';
  passwordConfirmation: string = '';
  loading: boolean = false;
  constructor(private authService: AuthService, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  register() {
    this.loading = true;
    this.errors = [];

    if (this.password !== this.passwordConfirmation) {
      this.errors.push('As Passwords não são iguais');
      this.loading = false;
      return;
    }

    let element = <HTMLInputElement> document.getElementById("agree");

    if (!element.checked) {
      this.errors.push('Não aceitou os termos e condições');
      this.loading = false;
      return;
    }

    this.http.post('auth/register', { email: this.email, name: this.name, password: this.password, role: 'utente' }).subscribe(
      (res: any) => {
        this.authService.setToken(res.token);
        this.toastr.success('Conta criada com sucesso!');
        this.router.navigate(['/']);
        this.loading = false;
      },
      err => {
        for (let error of err.error.data) {
          this.errors.push(error.message);
        }
        this.loading = false;
      }
    );
  }

}
