import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ExtrasComponent } from './extras.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Extras'
        },
        component: ExtrasComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExtrasRoutingModule { }
