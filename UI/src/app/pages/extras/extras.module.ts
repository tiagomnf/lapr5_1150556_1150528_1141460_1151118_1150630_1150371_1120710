import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SharedModule } from '../../shared/shared.module';


import { ExtrasRoutingModule } from './extras-routing.module';

import { ExtrasComponent } from './extras.component';



@NgModule({
    imports: [ ExtrasRoutingModule, SharedModule, ChartsModule],
    declarations: [ExtrasComponent]
})
export class ExtrasModule {}
