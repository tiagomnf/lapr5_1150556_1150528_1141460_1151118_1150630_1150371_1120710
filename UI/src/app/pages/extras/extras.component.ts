import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { Prescricao, Aviamento } from '../../models';   // modelos carregados

import * as moment from 'moment';


@Component({
  selector: 'app-extras',
  templateUrl: './extras.component.html',
  styleUrls: ['./extras.component.scss']
})
export class ExtrasComponent implements OnInit {

  pies  = {
      prescricoes: {
          labels: ["Domingo", "Segunda", "Terça","Quarta","Quinta","Sexta","Sábado"],
          data: [0, 0, 0,0, 0, 0,0],
          loading: false
      },
      topmedicamentos: {
          labels: ["", "", "",""],
          data: [0, 0, 0,0],
          loading: false
      }


  };

  constructor(
    private http: HttpClient,
    public authService: AuthService,
    private toastr: ToastrService

  ) { }


  ngOnInit() {

    this.loadData();

  }

  loadData() {
      this.load_7dias();
      this.load_top3();

  }

  private load_7dias(){


        this.pies.prescricoes.loading = true;
        this.http.get<any>('/chart/prescricoes/semana')
        .subscribe(
            response => {

              let data = [ response.domingo,response.segunda, response.terca,
             response.quarta,response.quinta,response.sexta,response.sabado];



              this.pies.prescricoes.data=data;


              this.pies.prescricoes.loading = false;


             },
             err => {
                 this.toastr.error(err.error.message, 'Erro');
                 this.pies.prescricoes.loading = false;
             }
        );
    }

    private load_top3(){


          this.pies.topmedicamentos.loading = true;
          this.http.get<any>('/chart/prescricoes/topmedicamentos')
          .subscribe(
              response => {

                let data = [ response.top1,response.top2, response.top3,
               response.resto];

               let labels = [response.str1,response.str2,response.str3,"Resto dos Medicamentos"];

               this.pies.topmedicamentos.data = data;
               this.pies.topmedicamentos.labels = labels;



               this.pies.topmedicamentos.loading = false;

               },
               err => {
                   this.toastr.error(err.error.message, 'Erro');
                   this.pies.prescricoes.loading = false;
               }
          );
      }

}
