import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptsComponent } from './receipts.component';
import { CreateReceiptsComponent } from './create-receipts/create-receipts.component';
import { EditReceiptComponent } from './edit-receipt/edit-receipt.component';
import { ReceiptDetailsComponent } from './receipt-details/receipt-details.component';
import { SearchReceiptComponent } from './search-receipt/search-receipt.component';

import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Receitas'
        },
        children: [{
            path: '',
            component: ReceiptsComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista',
                requiredRole: ['medico', 'utente']
            },
            canActivate: [AuthGuard]
        }, {
            path: 'criar',
            data: {
                title: 'Criar receita',
                requiredRole: ['medico']
            },
            component: CreateReceiptsComponent,
            canActivate: [AuthGuard]
        }, {
            path: 'procurar',
            canActivateChild: [AuthGuard],
            component: SearchReceiptComponent,
            data: {
                title: 'Pesquisar receita',
                requiredRole: ['farmaceutico']
            }
        }, {
            path: ':id/editar',
            data: {
                title: 'Editar',
                requiredRole: ['medico']
            },
            component: EditReceiptComponent,
            canActivate: [AuthGuard]
        }, {
            path: ':id/detalhes',
            data: {
                title: 'Detalhes',
                requiredRole: ['farmaceutico', 'utente']
            },
            component: ReceiptDetailsComponent,
            canActivate: [AuthGuard]
        }]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReceiptsRoutingModule {}
