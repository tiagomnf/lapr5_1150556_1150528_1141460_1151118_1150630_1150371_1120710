import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ExtrasMComponent } from './extras-m.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Extras'
        },
        component: ExtrasMComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExtrasMRoutingModule { }
