import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtrasMComponent } from './extras-m.component';

describe('ExtrasMComponent', () => {
  let component: ExtrasMComponent;
  let fixture: ComponentFixture<ExtrasMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtrasMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtrasMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
