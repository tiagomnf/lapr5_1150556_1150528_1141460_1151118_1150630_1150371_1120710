import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SharedModule } from '../../shared/shared.module';


import { ExtrasMRoutingModule } from './extras-m-routing.module';

import { ExtrasMComponent } from './extras-m.component';

@NgModule({
    imports: [ ExtrasMRoutingModule, SharedModule, ChartsModule],
    declarations: [ExtrasMComponent]
})
export class ExtrasMModule { }
